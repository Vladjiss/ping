#include <stdio.h>
#include <string>
#define MAX_ROWS 3
#define MAX_LENGTH 30
int ChangeRows(char** arr, int num){
	for(int i = 0; i < num; i++){
		int row_len = 0;
		while(arr[i][row_len]!='\0'||arr[i][row_len]!='\n'){
			row_len++;
		}
		arr[i] = Palindromize(arr[i],row_len,row_len);
	}
}
char* Palindromize(char* arr, int pos, int len){
	char *_arr = (char*)calloc(len, sizeof(char));
	if(pos>1) {
		char *_arr = Palindromize(arr, pos--, len);
	}
	_arr[pos-1]=arr[len-pos];
	return _arr;
}
int Matrix_print(char** arr, int num){
	for(int i = 0; i < num; i++){
		int j = 0;
		while(arr[i][j]!='\0'||arr[i][j]!='\n'){
			printf("%c", arr[i][j]);
			j++;
		}
	}
}
int main(){
	setlocale(LC_ALL,"Russian");
	char **arr;
	int num = 0;
	arr = (char**)calloc(num, sizeof(char*));
	printf("Enter rows");
	while(num<MAX_ROWS) {
		char str[MAX_LENGTH];
		fgets(str, MAX_LENGTH, stdin);
		if(str[0] == '\n' || str[0] == '\0')
			break;
		num++;
		arr = (char**)realloc(arr, num*sizeof(char*));
		arr[num-1] = (char*)calloc(MAX_LENGTH, sizeof(char));
		for(int i = 0; i < MAX_LENGTH+1; i++) {
			if(str[i] == '\n' || str[i] == '\0') 
				break;
			arr[num-1][i] = str[i];
		}
	}
	ChangeRows(arr, num);
	Matrix_print(arr, num);
	for(int i = 0; i < num; i++) 
		free(arr[i]);
	free(arr);
	return 0;
}
