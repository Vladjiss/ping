#include <stdio.h>
#include <string>
#include <time.h>
#define LENGTH 15
int max_len(int mass[], int length){
	int max = 1;
	int curr = 1;
	int i;
	for(i=1;i<length;i++){
		if(mass[i]==mass[i-1])
			curr++;
		else {
			curr=1;
		}
		if(curr>max)
			max = curr;
	}
	return max;
}
int main() {
	srand(time(0));
	int mass[LENGTH];
	int i;
	for (i=0; i<LENGTH; i++) {
		mass[i] = rand() % 4;
		printf("%i", mass[i]);
	}
	printf("\nMax: %d\n", max_len(mass, LENGTH));
	return 0;
}