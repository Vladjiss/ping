#include <stdio.h>
#include <string>
#include <locale.h>
int main() {
	setlocale(LC_ALL,"Russian");
	char str[256] = {0};
	char number[256] = {0};
	int sum=0;
	int i=0;
	int j=0;
	printf("Enter row\n");
	fgets(str,256,stdin);
	str[strlen(str)-1]=0;
	while(str[i]!='\0') {
		if(str[i]>='0' && str[i]<='9'){
			number[j]=str[i];
			j++;
		}
		else {
			if(j>0){
				sscanf(number,"%d", &j);
				sum+=j;
				j=0;
			}
		}
		i++;
	}
	if(j>0){
		sscanf(number,"%d", &j);
		sum+=j;
		j=0;
	}
	printf("%i\n",sum);
	return 0;
}