#include <stdio.h>
#include <string>
#include <time.h>
#define N 5
#define M 10
#define D 2
int maximum_repeat(int n, int m, char** arr){
	int row_num = 0;
	char** counter = new char*[N];
	int max_local;
	int max=0;
	for(int i = 0; i < n; i++) {
		counter[i] = new char[D];
		max_local=0;
		for(int j = 0; j < m; j++) {
			if(++counter[i][arr[i][j]]>max_local)
				max_local=counter[i][arr[i][j]];
		}
		if(max_local>max){
			max=max_local;
			row_num=i;
		}
	}
	return row_num;
}
int main(){
	setlocale(LC_ALL,"Russian");
	srand(time(0));
	char** mass = new char*[N];
	for(int i = 0; i < N; i++) {
		mass[i] = new char[M];
		for(int j = 0; j < M; j++) {
			mass[i][j] = rand()%D+1;
			printf("%c", mass[i][j]);
		}
		printf("\n");
	}	
	printf("max repeats in row %i\n",maximum_repeat(N,M,mass));
}