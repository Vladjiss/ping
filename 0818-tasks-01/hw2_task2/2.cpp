#include <stdio.h>
#include <string>
int isPalindrom(char mass[], int length){
	int isPalindrom = 1;
	int i=0;
	while(isPalindrom && i<length/2){
		if(mass[i]!=mass[length-i-1])
			isPalindrom = 0;
		i++;
	}
	return isPalindrom;
}
int main() {
	char str[256] = {0};
	char str_clear[256] = {0};
	int i=0;
	int length = 0;
	printf("Enter row\n");
	fgets(str,256,stdin);
	str[strlen(str)-1]=0;
	while(str[i]!='\0'){
		str_clear[i] = str[i];
		i++;
		length++;
	}
	printf("is palindrom: %d\n", isPalindrom(str_clear, length));
	return 0;
}