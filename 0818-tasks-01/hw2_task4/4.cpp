#include <stdio.h>
#include <string>
#include <time.h>
#define N 5
#define M 5
#define D 5
int maximum_sum(int n, int m, int** arr){
	int column_num = 0;
	int max=0;
	int sum_local=0;
	for(int j = 0; j < M; j++) {
		sum_local = 0;
		for(int i = 0; i < M; i++)
			sum_local+=arr[i][j];
		if(sum_local>max){
			max=sum_local;
			column_num=j;
		}
	}
	return column_num;
}
int main(){
	setlocale(LC_ALL,"Russian");
	srand(time(0));
	int** mass = new int*[N];
	for(int i = 0; i < N; i++) {
		mass[i] = new int[M];
		for(int j = 0; j < M; j++) {
			mass[i][j] = rand()%D+1;
			printf("%i", mass[i][j]);
		}
		printf("\n");
	}	
	printf("max sum in column %i\n",maximum_sum(N,M,mass));
}