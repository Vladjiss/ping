#include <stdio.h>
#include <string>
#include <locale.h>
int main() {
	setlocale(LC_ALL,"Russian");
	char str[256] = {0};
	int numbers_count=0;
	int i=0;
	int error=0;
	printf("Enter row\n");
	fgets(str,256,stdin);
	str[strlen(str)-1]=0;
	while(str[i]!='\0') {
		if(str[i]<'0' || str[i]>'9'){
			error = 1;
			i = strlen(str)-1;
		}
		else {
			numbers_count++;
		}
		i++;
	}
	if(error)
		printf("Input error\n");
	else {
		for(i=0;i<numbers_count;i++) {
			if(i>0 && i%3==numbers_count%3)
				printf(" ");
			printf("%c",str[i]);
		}
		puts("\n");
	}
	return 0;
}